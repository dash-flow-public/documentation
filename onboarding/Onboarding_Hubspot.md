# Onboarding Hubspot CRM

This tutorial assumes that for every new lead request a new "deal" is created in Hubspot that is connected to a contact.

The contact can be created through [Hubspot forms](https://www.hubspot.com/products/marketing/forms), [Hubspot API](https://developers.hubspot.com/docs/reference/api/crm/objects/contacts) or external tools like [zapier.com](https://zapier.com/)

In most cases, contacts are created via [Hubspot forms](https://www.hubspot.com/products/marketing/forms) and a newly created field ```ga4_id``` is configured as hidden field on the form to be filled.

## 1. Prepare Hubspot to collect GA4-user-id

### a. create new field on deal object

* go to ```Profile & Preferences```
* Under ```Data Management``` click ```Properties```
* Select ```Deal properties```
* ```Create property```
    * Property label: ```ga4_id```
    * Field type: ```Single-line text```

### b. create new field on contact object

* Select ```Contact properties```
* ```Create property```
    * Property label: ```ga4_id```
    * Field type: ```Single-line text```


### c. create workflow to copy property from contact to deal

* create an internal or external workflow that ensures that the value of ```ga4_id``` on the contact is copied to the associated value ```ga4_id``` on the deal exactly after or together with creation of the deal object.

* this depends on your specific setup: in case a Hubspot form creates a contact, you can create the following workflow

```
1. Contact-based workflow
2. Trigger: Has completed: Form submission
3. Action: Create deal record
4. ga4_id: copy from the contact property to deal.ga4_id
```


## 2. Connect Hubspot CRM to dashflow

### a. Hubspot: Create Private App

* login to your Hubspot Account
* go to ```Profile & Preferences```. Under ```Àccount Managament/Integrations``` click ```Private Apps```
* click ```Create private app```
    * Name: ```dashflow```
    * Scope: ```crm.export```, ```crm.objects.deals.read```, ```crm.objects.users.read```
* copy the Auth ```Access Token``` 

### b. dashflow: create connection

* visit https://app.dash-flow.io/dashboard-data and create and activate ```Hubspot connector```
* Sign-In and paste
```
{
    "access_token":"YOUR_ACCESS_TOKEN"
}
```

* Add ```Data Entity```
    * Name: ```deals```
    * Id: ```deals```
    * Currency: ```YOUR_HUBSPOT_CURRENCY```
    * Timezone: ```YOUR_HUBSPOT_TIMEZONE```
* Add ```Settings-Expression```
```
{
            "start_date": "<DATE>",
            "language": "EN",
            "objectType": "deals",
            "objectProperties": [
                "hs_object_id",
                "createdate",
                "closedate",
                "hs_is_closed_won",
                "hs_is_closed_lost",
                "dealname",
                "amount",
                "dealstage",
                "dealtype",
                "pipeline",
                "hubspot_owner_id",
                "ga4_id",
                "closed_lost_reason"
            ]
}
```