# Documentation

## Contents

- [Data storage](#data-storage)
- [Data access: Python](#data-access-python)
- [Data access: Power-BI](#data-access-power-bi)
- [Data access: Looker Studio](#data-access-looker-studio)
- [Data models](#data-models)

## Data storage

All data is stored safely on the [Microsoft Azure Cloud](https://learn.microsoft.com/en-us/azure/storage/blobs/data-lake-storage-introduction) with your access tokens protected by [Key-Vault-Technology](https://azure.microsoft.com/en-us/products/key-vault).

All data is stored in single [parquet files](https://www.databricks.com/glossary/what-is-parquet).

## Data access: Python

1. Login to https://app.dash-flow.io 

2. navigate to https://app.dash-flow.io/dashboard-data-settings and obtain STORAGE_URL and SAS_TOKEN

3. write your script

```python
# pip install pandas polars altair
import polars as pl
import pandas as pd
import altair as alt
SAS_TOKEN = "..."
STORAGE_URL="..."
url = f"{STORAGE_URL}/facts/base.parq"
url_with_sas = f"{url}?{SAS_TOKEN}"

# load data via pandas from url
df = pd.read_parquet(url_with_sas)
print(df.describe())

# convert to polars dataframe for speedup
df=pl.from_pandas(df)

# aggregate the data
aggregated_data = (
    df.with_columns((pl.col('date').dt.truncate('1mo')).alias('month'))
      .group_by(['channel', 'month'])
      .agg(pl.col('clicks').sum().alias('total_clicks'))
).with_columns(
    pl.col("channel").fill_null("No Channel").alias("channel"),
    pl.col("total_clicks").fill_null(0).alias("total_clicks")
)

# build altair chart
chart = alt.Chart(aggregated_data).mark_line().encode(
    x='month:T',
    y='total_clicks:Q',
    color='channel:N'
).properties(
    width=800,
    height=200,
    title='Total Clicks by Channel Over Time'
)

display(chart)
```

## Data access: Power-BI

1. Request for Power BI Template: send e-email to request a Power-BI template.

```
hello@dash-flow.io
```

2. Install [Power-BI Desktop](https://www.microsoft.com/de-de/power-platform/products/power-bi/downloads) .

3. Open customv1.pbit (template file). You should see a prompt with configuration settings.

![powerbi_config.webp](./images/powerbi_config.webp)

4. Set the **currency**: set according to your currency selection in https://app.dash-flow.io/dashboard-data-settings (for example USD)

5. Set **container**: visit https://app.dash-flow.io/dashboard-data-settings, select "SAS_CONTAINER" in the drop down and click the eye icon. Copy this value into the container field in Power BI. Leave the base value as it is. 

6. Set **SAS Token**: after step 5 you see a login screen, then click on "Shared Access Signature (SAS)" on the left side. Visit https://app.dash-flow.io/dashboard-data-settings, select "SAS_TOKEN" and click on the eye icon. Paste this value into the "Token" field in PowerBI.

![credentials_setting.webp](./images/credentials_setting.webp)

## Data access: Looker Studio

currently not planned, Looker Studio cannot handle enough data-rows in memory!

## Data models

### model-base

```
This model is a dynamic data model commonly used for business intelligence use cases. 
```

* {STORAGE_URL}/facts/base.parq?{SAS_TOKEN}

| **field_name** | **data_type** | **category** | **description** | **example** |
| --- | --- | --- | --- | --- |
| account_name | string | Dimension | Name of the advertising account | mybusiness.com |
| ad_id | string | Dimension | Unique identifier for the ad | ad_12345 |
| adgroup_id | string | Dimension | Unique identifier for the ad group | ag_67890 |
| adgroup_name | string | Dimension | Name of the ad group | Summer Sale Group |
| average_position | Float32 | Metric | Average position of the ad on the page | 1.5 |
| campaign_id | string | Dimension | Unique identifier for the campaign | camp_54321 |
| campaign_name | string | Dimension | Name of the campaign | Summer Sale Campaign |
| channel | string | Dimension | Marketing channel (e.g., organic, paid) | paid |
| clicks | UInt32 | Metric | Number of clicks on the ad | 150 |
| clicks_link | UInt32 | Metric | Number of clicks on links within the ad | 120 |
| comments | UInt32 | Metric | Number of comments on the ad | 30 |
| conversions | UInt32 | Metric | Sessions that converted on website (custom definition) | 25 |
| cost | Float32 | Metric | Cost of the ad | 200.5 |
| country | string | Dimension | Country where the ad was seen | USA |
| creative_id | string | Dimension | Unique identifier for the creative | crv_12345 |
| creative_img_url | string | Dimension | URL of the creative image | https://example.com/image.jpg |
| creative_name | string | Dimension | Name of the creative | Summer Sale Banner |
| creative_text | string | Dimension | Text of the creative | Get 50% off on all items! |
| creative_text_url | string | Dimension | URL within the creative text | https://example.com/deal |
| creative_title | string | Dimension | Title of the creative | Summer Sale |
| creative_type | string | Dimension | Type of creative (e.g., image, video) | image |
| creative_url | string | Dimension | URL to the creative | https://example.com/creative |
| creative_video_url | string | Dimension | URL of the creative video | https://example.com/video.mp4 |
| currency | string | Dimension | Currency of the cost | USD |
| created_time | datetime | Dimension | Time when the creative was created | 2023-04-01 12:00:00 |
| date | date | Dimension | Date of the data point | 01.04.2023 |
| device | string | Dimension | Device type (e.g., mobile, desktop) | mobile |
| domain | string | Dimension | Domain where the ad was shown | example.com |
| engaged_sessions | UInt32 | Metric | Engaged sessions (GA4 standard value) | 75 |
| engagement_creative | UInt32 | Dimension | Engagements with the creative (likes + shares + comments) | 40 |
| engagement_duration | UInt32 | Metric | Duration of engagement in seconds (GA4 standard value) | 120 |
| engagements | UInt32 | Metric | Sessions that engaged with website (custom definition) | 300 |
| event_name | string | Dimension | Name of the event triggered | purchase |
| impressions | UInt32 | Metric | Number of times the ad was shown | 1000 |
| keyword | string | Dimension | Keyword targeted by the ad | summer sale |
| keyword_groups | string | Dimension | Groups of keywords targeted | summer, sale, deals |
| likes | UInt32 | Metric | Number of likes on the ad | 200 |
| network | string | Dimension | Network where the ad was shown | Google |
| page_engagement | UInt32 | Metric | Number of engagements on the page | 500 |
| page_views | UInt32 | Metric | Number of page views | 800 |
| post_reactions | UInt32 | Metric | Number of reactions to a post | 100 |
| region | string | Dimension | Region where the ad was seen | California |
| sessions | UInt32 | Metric | Number of sessions | 250 |
| new_users | UInt32 | Metric | Number of first-time-visitors | 100 |
| shares | UInt32 | Metric | Number of shares of the ad | 50 |
| site_source_name | string | Dimension | Name of the site source | Google Ads |
| source_medium | string | Dimension | Source and medium of the traffic | google/cpc |
| tracking_status | string | Dimension | Status of the tracking | active |
| uri | string | Dimension | URI of the page visited | /products/summer-sale |
| url | string | Dimension | Full URL of the page visited | https://example.com/products/summer-sale |
| utm_campaign | string | Dimension | UTM parameter for the campaign | summer2023 |
| utm_content | string | Dimension | UTM parameter for the content | banner_ad |
| utm_medium | string | Dimension | UTM parameter for the medium | cpc |
| utm_source | string | Dimension | UTM parameter for the source | google |
| utm_term | string | Dimension | UTM parameter for the keyword term | discount+summer |
| user_id | string | Dimension | Custom defined user id for crm connection | GA1.1123.12412523546 |
| user_sessions | UInt32 | Metric | Session segmented on userId level | 33 |
| video_views | UInt32 | Metric | Number of time a video was viewed | 30 |
| data_source | string | Dimension | Source of connected data | Google Analytics 4 |
| channel_conversions | UInt32 | Metric | Conversions collected by channel specific tracking pixels | 33 |